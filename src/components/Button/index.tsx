import { useState, useRef, useEffect } from 'react'

interface ButtonProps {
  startAnim(): void
}

export const Button = (props: ButtonProps) => {
  const { startAnim } = props
  const intervalRef = useRef({})
  const [time, setTime] = useState(0)
  const [isStart, setIsStart] = useState(false)

  useEffect(() => {
    return () => {
      clearInterval(intervalRef.current)
    }
  }, [])

  const startUITime = () => {
    intervalRef.current = setInterval(() => {
      setTime((prev: number) => {
        if (prev !== 0) {
          return prev - 1000
        } else {
          clearInterval(intervalRef.current)
          setIsStart(false)
          return 0
        }
      })
    }, 1000)
  }

  const onClick = () => {
    if (!isStart) {
      setIsStart(true)
      setTime(5000)
      startUITime()
      startAnim()
    }
  }

  const parseTime = (time: number) => `${time / 1000}`

  return (
    <button onClick={() => onClick()}>{!isStart ? 'Start' : parseTime(time)}</button>
  )
}
