import React, { Ref } from 'react'
import './circle.css'

interface CircleProps {
  style: object
}

export const Circle = React.forwardRef((props: CircleProps, ref: Ref<HTMLDivElement>) => (
  <div ref={ref} className='circle' style={props.style} />
))
