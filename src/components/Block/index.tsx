import { Ref, forwardRef } from 'react'
import './Block.css'

interface BlockProps {
  title: string
  anim?: boolean
}

export const Block = forwardRef((props: BlockProps, ref: Ref<HTMLDivElement>) => {
  const { title, anim } = props
  return (
    <div ref={ref} className={anim ? 'block anim-block' : 'block'}>
      {title}
    </div>
  )
})
