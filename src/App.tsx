import { useRef, useState } from 'react'
import { Circle, Button, Block } from './components'
import './App.css'

function App() {
  const block1Ref = useRef(null)
  const block2Ref = useRef(null)
  const circleRef = useRef(null)
  const [circleStyle, setCircleStyle] = useState({
    top: 0,
    left: 0,
    display: 'none'
  })

  const getPosition = (elem: any) => {
    const elemPos = elem.current.getBoundingClientRect()
    const topC = elemPos.top + (elemPos.height / 2) - 25
    const leftC = elemPos.left + (elemPos.width / 2) - 25
    return { topC, leftC }
  }

  const linear = (timeFraction: number) => {
    return timeFraction
  }

  const startAnim = () => {
    const startPos = getPosition(block1Ref)
    setCircleStyle({ top: startPos.topC, left: startPos.leftC, display: 'block' })

    animate({
      duration: 2000,
      timing: linear,
      draw: function (progress: number) {
        setCircleStyle((prev) => {
          if (progress === 1) {
            return {
              top: ((getPosition(block2Ref).topC - startPos.topC) * progress) + startPos.topC,
              left: prev.left, display: 'none'
            }
          }
          return {
            top: ((getPosition(block2Ref).topC - startPos.topC) * progress) + startPos.topC,
            left: prev.left, display: 'block'
          }
        })
      }
    })

    animate({
      duration: 2000,
      timing: linear,
      draw: function (progress: number) {
        setCircleStyle((prev) => {
          if (progress === 1) {
            return {
              top: prev.top,
              left: ((getPosition(block2Ref).leftC - startPos.leftC) * progress) + startPos.leftC, display: 'none'
            }
          }
          return {
            top: prev.top,
            left: ((getPosition(block2Ref).leftC - startPos.leftC) * progress) + startPos.leftC, display: 'block'
          }
        })
      }
    })
  }

  const animate = (options: any) => {
    const start = performance.now()

    requestAnimationFrame(function animate(time) {
      let timeFraction = (time - start) / options.duration
      if (timeFraction > 1) timeFraction = 1

      const progress = options.timing(timeFraction)

      options.draw(progress)

      if (timeFraction < 1) {
        requestAnimationFrame(animate)
      }
    })
  }

  return (
    <div className='App'>
      <div className='cont-blocks'>
        <Block title='1' ref={block1Ref} anim />
        <Block title='2' ref={block2Ref} />
        <Circle ref={circleRef} style={circleStyle} />
      </div>
      <Button startAnim={() => startAnim()} />
    </div>
  )
}

export default App
